# -*- coding: utf-8 -*-
extensions = [
    'sphinx.ext.todo',
]

#templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'README'

project = u'Minimální distribuce Sonata aplikace'
copyright = u'2015, Martin Hasoň'

version = '0.9'
release = '0.9@beta'

language = 'cs'

#today = ''
#today_fmt = '%B %d, %Y'

exclude_patterns = ['build', 'var', 'vendor']

#default_role = None
#show_authors = False

html_theme = 'sphinx_rtd_theme' #alabaster, sphinx_rtd_theme
pygments_style = 'colorful'

